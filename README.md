[![pipeline status](https://git.thm.de/matrix/Dokumentation/badges/thm-main/pipeline.svg)](https://git.thm.de/matrix/Dokumentation/-/commits/thm-main) 

# Technische Hochschule Mittelhessen Matrix Dokumentation
**Website:** **https://matrix-service.thm.de/docs/**

### To build:
1. Clone the repository `git clone --recurse-submodules git@git.thm.de:matrix/Dokumentation.git`
1. `cd Dokumentation`
1. Install [Hugo](https://gohugo.io/getting-started/installing)
1. Run `hugo server -D`
1. Open http://localhost:1313/

### Credits of this Documentation
This documentation has been created based on the Matrix documentation of [TU Dresden](https://doc.matrix.tu-dresden.de/) which was published using a [Creative Commons license](https://git.thm.de/matrix/Dokumentation/-/blob/thm-main/LICENSE.md).
The source documentation of TU Dresden can be found at [github](https://github.com/matrix-tu-dresden-de/Dokumentation).
