---
title: "Datenschutz"
date: 2020-07-03T13:25:27+02:00
draft: false
chapter: true
weight: 900
---
## Datenschutzerklärung für den Matrix-Dienst der THM

**Welcher Zweck wird mit der Datenverarbeitung verfolgt?**

"Matrix" ist ein offener, dezentraler Kommunikationsdienst für die Echtzeitkommunikation. Es wird den Mitgliedern und Angehörigen der THM unter Einhaltung der einschlägigen gesetzlichen und rechtlichen Bestimmungen zum Datenschutz und zur IT-Sicherheit ermöglicht, mittels ihrer THM-Benutzerkennung mit Angehörigen dieser und anderer Universitäten sowie weiteren Matrix-Nutzenden (bspw. akademischen Partner:innen) per Chat sowie Audio-/Video-Telefonie zu kommunizieren. Ziel des Einsatzes von Groupware-Systemen sind insbesondere die Sicherstellung und Vereinfachung arbeitsorganisatorischer Maßnahmen für die gemeinschaftliche Arbeit von Nutzerinnen und Nutzern, Personengruppen, Teams und Gremien sowie das Kommunikationsmanagement. Die Verarbeitung personenbezogener Daten erfolgt ausschließlich für vorgenannte Zwecke.

**Wer ist für die Datenverarbeitung verantwortlich und an wen können sich
betroffene wenden?**

Verantwortlicher
i.S.d. Art. 4 Nr. 7 DSGVO ist die [THM](https://thm.de/impressum).

Ansprechpartner:

TBD

[matrix@thm.de](mailto:matrix@thm.de)

sowie der Datenschutzbeauftragte der THM

E-Mail: [datenschutz@thm.de](mailto:datenschutz@thm.de)
(https://www.thm.de/datenschutz/)


**Welche personenbezogenen Daten werden verarbeitet?**

Die Verarbeitung umfasst folgende personenbezogene Daten:

1. Zugangsverwaltung: Vor- & Nachname(n), Mailadresse, Matrix-ID (Localpart der
    Mail-Adresse), Anzeigename

2. Authentifizierung: Nutzername und Passwort

3. Benutzerinhalte: alle Daten, welche der Nutzer in das System eingibt (Ende-zu-Ende-verschlüsselt möglich)

4. Geräteidentifikation: IP-Adressen mit Zeitstempel und Gerätename; verwendete Art des Endgerätes (Mobil / Desktop), Betriebssystem

5. Serverprotokoll: IP-Adressen mit Zeitstempel

6. Audio-/Video-Telefonie: IP-Adressen, AV-Daten

7. Benachrichtigungen (Mail)

**Wie lange werden die personenbezogenen Daten gespeichert?**

Die personenbezogenen Daten werden gemäß § 16 Abs. 4 IT-Ordnung der TU
Dresden spätestens 15 Monate nach dem Ausscheiden der betroffenen
Person gelöscht.

**Welche Rechte stehen den betroffenen Personen grundsätzlich zu?**

<u>Auskunftsrecht (Art. 15 DSGVO)</u>

Die Betroffenen haben das Recht, jederzeit Auskunft über die zu ihrer
Person verarbeiteten Daten sowie die möglichen Empfänger dieser
Daten verlangen zu können.

<u>Recht auf Berichtigung, Löschung und Einschränkung (Art. 16 - 18 DSGVO)</u>

Die Betroffenen können jederzeit gegenüber der THM die
Berichtigung, Löschung ihrer personenbezogenen Daten bzw. die
Einschränkung der Verarbeitung zu verlangen.

<u>Recht auf Datenübertragbarkeit (Art. 20 DSGVO)</u>

Die Betroffenen können verlangen, dass der Verantwortliche ihnen ihre
personenbezogenen Daten maschinenlesbaren Format übermittelt.

<u>Beschwerderecht (Art. 77 DSGVO)</u>

Betroffene Personen können sich jederzeit an die zuständige Aufsichtsbehörde
für den Datenschutz wenden. Die zuständige Aufsichtsbehörde ist:

Hessischer Datenschutzbeauftragter

TBD

TBD

Tel.: TBD

Fax: TBD

E-Mail: [TBD](mailto:TBD)


[Impressum]({{< ref "imprint" >}})


