---
title: "Impressum"
date: 2020-07-02T21:22:54+02:00
draft: false
chapter: true
weight: 1000
---
## Impressum

Es gilt das [Impressum der THM](https://www.thm.de/site/impressum.html) mit folgenden Änderungen:

### Ansprechpartner:

#### Prof. Dr. Oliver Sander 

Professur für Numerik partieller Differentialgleichungen 

THM 

01062 Dresden 

### sowie

#### Christian Bruchatz 

Bereich Ingenieurwissenschaften

01062 Dresden 

Tel.    +49 351 463-32328 

matrix@thm.de 

### Technische Umsetzung:

#### Christoph Johannes Kleine

Zentrum für Informations­dienste und Hochleis­tungsrechnen (ZIH)

matrix@thm.de 

#### Sebastian Itting

Centre for Tactile Internet with Human-in-the-Loop 

CeTI - Cluster of Excellence 

01062 Dresden 

Tel. +49 351 463-42066

sebastian.itting@thm.de


## [Datenschutzerklärung]({{< relref "privacy">}})


