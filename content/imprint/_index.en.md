---
title: "Imprint"
date: 2020-07-02T21:22:54+02:00
draft: false
chapter: true
weight: 1000
---
## Imprint

The [Imprint of the THM](https://www.thm.de/site/impressum.html) is valid with the following changes:

### Contact person:

#### Prof. Dr. Oliver Sander 

Professorship for Numerics of Partial Differential Equations 

THM 

01062 Dresden 


#### Christian Bruchatz 

School of Engineering Sciences

01062 Dresden 

Phone +49 351 463-32328 

matrix@thm.de 

### Technical implementation:

#### Christoph Johannes Kleine

Center for Information Services and High Performance Computing (ZIH)

matrix@thm.de 

#### Sebastian Itting

Centre for Tactile Internet with Human-in-the-Loop 

CeTI - Cluster of Excellence 

01062 Dresden 

Phone +49 351 463-42066

sebastian.itting@thm.de


## [privacy policy]({{< relref "privacy">}})


